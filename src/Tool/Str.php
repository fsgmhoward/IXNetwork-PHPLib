<?php
/*
 * String Class
 * Written by Howard Liu <howard@ixnet.work>
 * Licensed under MIT
 */

namespace IXNetwork\Lib\Tool;

class Str
{
    /**
     * Generate a random code to separate different sessions
     *
     * @param  int $codeLength
     * @param  int $returnLength
     * @return string
     */
    public static function random($codeLength = 16, $returnLength = 6)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|';
        $code ='';
        for ($i=0; $i<=$codeLength; $i++) {
            $code .= $chars[rand(0, strlen($chars)-1)];
        }
        return substr(md5($code), 0, $returnLength);
    }
}
