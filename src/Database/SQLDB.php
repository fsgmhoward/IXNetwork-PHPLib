<?php
/*
 * SQLDB Database Factory Class
 * Written by Howard Liu <howard@ixnet.work>
 * Licensed under MIT
 */

namespace IXNetwork\Lib\Database;

class SQLDatabaseFactory
{
    public static function construct($driver = 'mysql', $argv = [])
    {
        switch ($driver) {
            case 'mysql':
                $dbHost = isset($argv['host']) ? $argv['host'] : 'localhost';
                $dbUser = isset($argv['user']) ? $argv['user'] : 'root';
                $dbPass = isset($argv['pass']) ? $argv['pass'] : '';
                $dbName = isset($argv['name']) ? $argv['name'] : 'IX_Network';
                if (extension_loaded('mysqli')) {
                    return new Database\MySQLi($dbHost, $dbUser, $dbPass, $dbName);
                } else {
                    return new Database\MySQL($dbHost, $dbUser, $dbPass, $dbName);
                }
                // no break because no need to break
            default:
                return false;
        }
    }
}
